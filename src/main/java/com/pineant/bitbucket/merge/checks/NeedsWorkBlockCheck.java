package com.pineant.bitbucket.merge.checks;

import com.atlassian.bitbucket.hook.repository.*;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.i18n.I18nService;

import org.springframework.beans.factory.annotation.Autowired;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;

import java.util.*;
import javax.annotation.Nonnull;

@Component("needsWorkBlocker")
public class NeedsWorkBlockCheck implements RepositoryMergeCheck {
    private final I18nService i18nService;

    @Autowired
    public NeedsWorkBlockCheck(@ComponentImport I18nService i18nService) {
        this.i18nService = i18nService;
    }
    @Nonnull
    @Override
    public RepositoryHookResult preUpdate(@Nonnull PreRepositoryHookContext context,
                                          @Nonnull PullRequestMergeHookRequest request) {

    	// Get all Pull Request reviewers
        Set<PullRequestParticipant> pullReqParticipants = new HashSet<PullRequestParticipant>();
        pullReqParticipants = request.getPullRequest().getReviewers();

        // Setup the Merge block messages
        String summaryMsg = i18nService.getText("com.pineant.bitbucket.needs-work-check.summary",
                "Needs Work Blocker");
        String detailedMsg = i18nService.getText("com.pineant.bitbucket.needs-work-check.message",
        		"There are still review statuses in Needs Work for this PR. Merging is not allowed until they are fixed!");

        // Loop through all reviewers and reject if one of them is set to Needs work
		for (PullRequestParticipant reviewer : pullReqParticipants) {
			if(PullRequestParticipantStatus.NEEDS_WORK.equals(reviewer.getStatus())){
				return RepositoryHookResult.rejected(summaryMsg, detailedMsg);
			}
		}
		// All good! Let's accept the Merge
        return RepositoryHookResult.accepted();
    } 
}